import logging
from flask import Flask, render_template, request
from collections import defaultdict

appServer = Flask(__name__)
user_ips = defaultdict(int)

@appServer.route('/')
def index():
    logging.debug('Ip request: %s', request.remote_addr)
    user_ips[request.remote_addr] += 1
    num_users = len(user_ips)
    return render_template('index.html', num_users=num_users, user_ips=user_ips)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    appServer.config['DEBUG'] = True
    appServer.run()
